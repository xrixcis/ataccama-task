package com.gitlab.xrixcis.ataccama.service;

import com.gitlab.xrixcis.ataccama.view.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DatabaseMetadataServiceTest {

    @Autowired
    private DatabaseMetadataService service;

    @MockBean
    private ConnectionService mockConnectionService;

    @Autowired
    private DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        when(mockConnectionService.getDataSource(42L))
                .thenReturn(dataSource);
    }

    @Test
    public void listSchemas() {
        List<String> schemas = service.listSchemas(42L);

        assertEquals(Arrays.asList("information_schema", "pg_catalog", "public"), schemas);
    }

    @Test
    public void getSchema() {
        SchemaView schema = service.getSchema(42L, "public");

        assertEquals("public", schema.getName());
        assertEquals(Collections.singletonList(new TableView("connection", "TABLE")), schema.getTables());
    }

    @Test
    public void getTableColumns() {
        TableView table = service.getTable(42L, "public", "connection");

        assertEquals("connection", table.getName());
        assertEquals("TABLE", table.getType());
        assertNotNull(table.getColumns());
        List<String> columns = table.getColumns().stream().map(ColumnView::getName).sorted().collect(Collectors.toList());
        List<String> expectedColumns = Arrays.asList("database", "host", "id", "name", "password", "port", "username");

        assertEquals(expectedColumns, columns);
    }

    @Test
    public void getTableColumnTypes() {
        TableView table = service.getTable(42L, "public", "connection");

        assertNotNull(table.getColumns());
        Optional<ColumnView> idColumn = table.getColumns().stream().filter(col -> "id".equals(col.getName())).findFirst();
        assertTrue(idColumn.isPresent());
        assertEquals("BIGINT", idColumn.get().getJdbcType());
        assertEquals("int8", idColumn.get().getType());
        assertEquals(19, idColumn.get().getSize());
        assertFalse(idColumn.get().isNullable());
        assertNull(idColumn.get().getDefaultValue());
    }

    @Test
    public void getTableColumnsPK() {
        TableView table = service.getTable(42L, "public", "connection");

        List<ColumnView> pks = table.getColumns().stream().filter(ColumnView::isPrimaryKey).collect(Collectors.toList());
        assertEquals(1, pks.size());
        assertEquals("id", pks.get(0).getName());
    }

    @Test
    public void getTableIndices() {
        TableView table = service.getTable(42L, "public", "connection");

        List<IndexView> indices = table.getIndices();
        assertEquals(2, indices.size());
        Optional<IndexView> name = indices.stream().filter(i -> i.getColumns().contains("name")).findFirst();
        assertTrue(name.isPresent());
        assertTrue(name.get().isUnique());
        assertEquals(Collections.singletonList("name"), name.get().getColumns());
    }

    /**
     * TODO: depends on the data in the database, will be a problem if the table is empty
     */
    @Test
    public void getTableData() {
        TableDataView table = service.getTableData(42L, "public", "connection", 10);

        assertEquals("connection", table.getName());
        assertEquals("public", table.getSchema());
        assertFalse(table.getData().isEmpty());
    }
}