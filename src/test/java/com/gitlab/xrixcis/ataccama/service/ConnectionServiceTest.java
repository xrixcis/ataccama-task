package com.gitlab.xrixcis.ataccama.service;

import com.gitlab.xrixcis.ataccama.entity.Connection;
import com.gitlab.xrixcis.ataccama.repository.ConnectionRepository;
import com.gitlab.xrixcis.ataccama.view.ConnectionView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceTest {

    @InjectMocks
    private ConnectionService service;

    @Mock
    private ConnectionRepository mockRepository;

    private static Connection createConnection() {
        return new Connection("test", "local", 2, "db", "user", "pw");
    }

    @Test
    public void listAll() {
        Connection connection = createConnection();
        when(mockRepository.findAll(any(Sort.class)))
                .thenReturn(Collections.singletonList(connection));

        List<ConnectionView> connections = service.listAll();
        assertEquals(1, connections.size());
        assertEquals(connection.getName(), connections.get(0).getName());
    }

    @Test
    public void get() {
        Connection connection = createConnection();
        when(mockRepository.getOne(10L))
                .thenReturn(connection);

        ConnectionView conn = service.get(10L);
        assertEquals(connection.getName(), conn.getName());
    }

    @Test
    public void store() {
        ConnectionView connection = new ConnectionView(createConnection());

        when(mockRepository.save(any(Connection.class)))
                .thenAnswer(invocation -> {
                    Connection conn = invocation.getArgument(0);
                    conn.setId(1000L);
                    return conn;
                });
        Long id = service.store(connection);
        assertEquals(Long.valueOf(1000L), id);
    }

    @Test
    public void update() {
        Connection connection = createConnection();
        connection.setId(1000L);

        when(mockRepository.getOne(1000L))
                .thenReturn(connection);

        ConnectionView view = new ConnectionView(connection);
        view.setName("updated");

        service.update(1000L, view);

        assertEquals(view.getName(), connection.getName());
    }

    @Test
    public void delete() {
        service.delete(100L);
        verify(mockRepository).deleteById(100L);
    }
}