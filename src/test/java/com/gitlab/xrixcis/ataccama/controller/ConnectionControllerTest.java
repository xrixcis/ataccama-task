package com.gitlab.xrixcis.ataccama.controller;

import com.gitlab.xrixcis.ataccama.service.ConnectionService;
import com.gitlab.xrixcis.ataccama.view.ConnectionView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConnectionController.class)
public class ConnectionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConnectionService mockService;

    @Test
    public void listConnections() throws Exception {
        ConnectionView connection = new ConnectionView();
        connection.setId(100L);
        when(mockService.listAll()).thenReturn(Collections.singletonList(connection));
        mvc.perform(get("/v1/connection"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", equalTo(100)));
    }

    @Test
    public void getConnection() throws Exception {
        ConnectionView connection = new ConnectionView();
        connection.setId(100L);
        connection.setName("Foo");
        when(mockService.get(100L)).thenReturn(connection);
        mvc.perform(get("/v1/connection/{id}", connection.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(100)))
                .andExpect(jsonPath("$.name", equalTo("Foo")));
    }

    // TODO: test all the validations
    @Test
    public void createConnectionValidates() throws Exception {

        mvc.perform(post("/v1/connection")
                .content("{\n" +
                        "  \"host\": \"localhost\",\n" +
                        "  \"port\": 5432,\n" +
                        "  \"database\": \"task\",\n" +
                        "  \"username\": \"postgres\",\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createConnection() throws Exception {

        mvc.perform(post("/v1/connection").content("{\n" +
                "  \"name\": \"test\",\n" +
                "  \"host\": \"localhost\",\n" +
                "  \"port\": 5432,\n" +
                "  \"database\": \"task\",\n" +
                "  \"username\": \"postgres\"\n" +
                "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(mockService).store(argThat(argument -> argument != null
                && "test".equals(argument.getName())
                && "localhost".equals(argument.getHost())
                && Integer.valueOf(5432).equals(argument.getPort())
                && "task".equals(argument.getDatabase())
                && "postgres".equals(argument.getUsername())));
    }

    @Test
    public void updateConnectionValidates() throws Exception {

        mvc.perform(put("/v1/connection/1")
                .content("{\n" +
                        "  \"host\": \"localhost\",\n" +
                        "  \"port\": 5432,\n" +
                        "  \"database\": \"task\",\n" +
                        "  \"username\": \"postgres\",\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void updateConnection() throws Exception {
        mvc.perform(put("/v1/connection/1").content("{\n" +
                "  \"name\": \"test\",\n" +
                "  \"host\": \"localhost\",\n" +
                "  \"port\": 5432,\n" +
                "  \"database\": \"task\",\n" +
                "  \"username\": \"postgres\"\n" +
                "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(mockService).update(eq(1L), argThat(argument -> argument != null
                && "test".equals(argument.getName())
                && "localhost".equals(argument.getHost())
                && Integer.valueOf(5432).equals(argument.getPort())
                && "task".equals(argument.getDatabase())
                && "postgres".equals(argument.getUsername())));
    }

    @Test
    public void deleteConnection() throws Exception {
        mvc.perform(delete("/v1/connection/{id}", 100))
                .andExpect(status().isOk());
    }
}