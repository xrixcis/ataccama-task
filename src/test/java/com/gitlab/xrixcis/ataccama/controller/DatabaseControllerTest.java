package com.gitlab.xrixcis.ataccama.controller;

import com.gitlab.xrixcis.ataccama.service.DatabaseMetadataService;
import com.gitlab.xrixcis.ataccama.view.ColumnView;
import com.gitlab.xrixcis.ataccama.view.IndexView;
import com.gitlab.xrixcis.ataccama.view.SchemaView;
import com.gitlab.xrixcis.ataccama.view.TableView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DatabaseController.class)
public class DatabaseControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DatabaseMetadataService mockService;

    static final long CONNECTION_ID = 666L;

    static final String SCHEMA_NAME = "brno";

    @Test
    public void listSchemasHandlesNotFound() throws Exception {
        when(mockService.listSchemas(CONNECTION_ID)).thenThrow(new EntityNotFoundException());

        mvc.perform(get("/v1/database/{id}/schema", CONNECTION_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void listSchemas() throws Exception {
        List<String> schemas = Arrays.asList("foo", "bar");
        when(mockService.listSchemas(CONNECTION_ID)).thenReturn(schemas);

        mvc.perform(get("/v1/database/{id}/schema", CONNECTION_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", equalTo("foo")))
                .andExpect(jsonPath("$[1]", equalTo("bar")));
    }

    @Test
    public void getSchemaHandlesNotFound() throws Exception {
        when(mockService.getSchema(CONNECTION_ID, SCHEMA_NAME)).thenThrow(new EntityNotFoundException());

        mvc.perform(get("/v1/database/{id}/schema/{schema}", CONNECTION_ID, SCHEMA_NAME))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getSchema() throws Exception {
        SchemaView schema = new SchemaView(SCHEMA_NAME, Collections.singletonList(new TableView("tabule", "TABLE")));
        when(mockService.getSchema(CONNECTION_ID, schema.getName())).thenReturn(schema);

        mvc.perform(get("/v1/database/{id}/schema/{schema}", CONNECTION_ID, schema.getName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(schema.getName())))
                .andExpect(jsonPath("$.tables", hasSize(1)))
                .andExpect(jsonPath("$.tables[0].name", equalTo("tabule")));
    }

    @Test
    public void getSchemaShortTableView() throws Exception {
        SchemaView schema = new SchemaView(SCHEMA_NAME, Collections.singletonList(new TableView("tabule", "TABLE")));
        when(mockService.getSchema(CONNECTION_ID, schema.getName())).thenReturn(schema);

        mvc.perform(get("/v1/database/{id}/schema/{schema}", CONNECTION_ID, schema.getName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tables[0].columns").doesNotExist())
                .andExpect(jsonPath("$.tables[0].indices").doesNotExist());
    }

    @Test
    public void getGetTable() throws Exception {
        TableView table = new TableView("tabule", "TABLE");
        when(mockService.getTable(CONNECTION_ID, SCHEMA_NAME, table.getName())).thenReturn(table);

        mvc.perform(get("/v1/database/{id}/schema/{schema}/table/{table}", CONNECTION_ID, SCHEMA_NAME, table.getName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(table.getName())))
                .andExpect(jsonPath("$.type", equalTo(table.getType())));
    }

    @Test
    public void getGetTableColumns() throws Exception {
        List<ColumnView> columns = Arrays.asList(
                new ColumnView("id", "int8", "BIGINT",  6, false, null, true),
                new ColumnView("name", "varchar", "VARCHAR",  6, false, null, false));
        TableView table = new TableView("tabule", "TABLE", columns, Collections.emptyList());
        when(mockService.getTable(CONNECTION_ID, SCHEMA_NAME, table.getName())).thenReturn(table);

        mvc.perform(get("/v1/database/{id}/schema/{schema}/table/{table}", CONNECTION_ID, SCHEMA_NAME, table.getName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.columns", hasSize(2)))
                .andExpect(jsonPath("$.columns[0].name", equalTo("id")))
                .andExpect(jsonPath("$.columns[0].primaryKey", equalTo(true)));
    }

    @Test
    public void getGetTableIndices() throws Exception {
        List<IndexView> indices = Arrays.asList(
                new IndexView("pk_idx", true, Arrays.asList("id", "b")),
                new IndexView("unq_idx", true, Collections.singletonList("name")));

        TableView table = new TableView("tabule", "TABLE", Collections.emptyList(), indices);
        when(mockService.getTable(CONNECTION_ID, SCHEMA_NAME, table.getName())).thenReturn(table);

        mvc.perform(get("/v1/database/{id}/schema/{schema}/table/{table}", CONNECTION_ID, SCHEMA_NAME, table.getName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indices", hasSize(2)))
                .andExpect(jsonPath("$.indices[0].name", equalTo("pk_idx")))
                .andExpect(jsonPath("$.indices[1].unique", equalTo(true)))
                .andExpect(jsonPath("$.indices[1].columns[0]", equalTo("name")));
    }
}