package com.gitlab.xrixcis.ataccama.service;

import com.gitlab.xrixcis.ataccama.entity.Connection;
import com.gitlab.xrixcis.ataccama.repository.ConnectionRepository;
import com.gitlab.xrixcis.ataccama.view.ConnectionView;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service working with Connections
 */
@Service
public class ConnectionService {

    private final ConnectionRepository repository;

    public ConnectionService(ConnectionRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public List<ConnectionView> listAll() {
        return repository.findAll(Sort.by("name"))
                .stream()
                .map(ConnectionView::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public ConnectionView get(Long id) {
        return new ConnectionView(repository.getOne(id));
    }

    @Transactional
    public Long store(ConnectionView connection) {
        Connection entity = new Connection(
                connection.getName(),
                connection.getHost(),
                connection.getPort(),
                connection.getDatabase(),
                connection.getUsername(),
                connection.getPassword()
        );
        return repository.save(entity).getId();
    }

    @Transactional
    public void update(Long id, ConnectionView connection) {
        Connection entity = repository.getOne(id);
        entity.setName(connection.getName());
        entity.setPort(connection.getPort());
        entity.setDatabase(connection.getDatabase());
        entity.setUsername(connection.getUsername());
        entity.setPassword(connection.getPassword());
    }

    @Transactional
    public void delete(Long id) {
        repository.deleteById(id);
    }

    /**
     * Returns a datasource providing a connection to a given database.
     *
     * @param connectionId
     * @return
     */
    @Transactional
    public DataSource getDataSource(Long connectionId) {
        Connection connection = repository.getOne(connectionId);
        String url = "jdbc:" + UriComponentsBuilder.newInstance()
                .scheme("postgresql") // TODO: different database vendors
                .host(connection.getHost())
                .port(connection.getPort())
                .path(connection.getDatabase())
                .toUriString();
        // TODO: datasource caching and connection pooling
        return new SingleConnectionDataSource(url, connection.getUsername(), connection.getPassword(), false);
    }
}
