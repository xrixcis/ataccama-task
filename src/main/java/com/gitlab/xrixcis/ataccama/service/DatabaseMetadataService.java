package com.gitlab.xrixcis.ataccama.service;

import com.gitlab.xrixcis.ataccama.view.*;
import org.postgresql.core.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service gathering database metadata.
 *
 * TODO: should sanitize the schema/table name arguments for % and _ wildcards
 */
@Service
public class DatabaseMetadataService {

    private static final String SCHEMA_NAME = "TABLE_SCHEM";

    private static final String TABLE_NAME = "TABLE_NAME";
    private static final String TABLE_TYPE = "TABLE_TYPE";

    private static final String COLUMN_NAME = "COLUMN_NAME";
    private static final String COLUMN_TYPE = "TYPE_NAME";
    private static final String COLUMN_JDBCTYPE = "DATA_TYPE";
    private static final String COLUMN_SIZE = "COLUMN_SIZE";
    private static final String COLUMN_NULLABLE = "IS_NULLABLE";
    private static final String COLUMN_DEFAULT = "COLUMN_DEF";

    private static final String NULLABLE_CONST = "YES";

    private static final String INDEX_NAME = "INDEX_NAME";
    private static final String INDEX_NON_UNIQUE = "NON_UNIQUE";

    private final ConnectionService connectionService;

    private final String[] tableTypes;

    public DatabaseMetadataService(ConnectionService connectionService, @Value("${list.table.types}") String[] tableTypes) {
        this.connectionService = connectionService;
        this.tableTypes = Arrays.copyOf(tableTypes, tableTypes.length);
    }

    public List<String> listSchemas(Long connectionId) {
        return runWithConnectionMeta(connectionId, meta -> mapResultSet(meta.getSchemas(), (rs, rowNum) -> rs.getString(SCHEMA_NAME)));
    }

    public SchemaView getSchema(Long connectionId, String schemaName) {
        List<TableView> tables = runWithConnectionMeta(connectionId, meta -> {
            ResultSet schemas = meta.getTables(null, schemaName, null, tableTypes);
            return mapResultSet(schemas, (rs, rowNum) ->
                    new TableView(rs.getString(TABLE_NAME), rs.getString(TABLE_TYPE)));
        });
        return new SchemaView(schemaName, tables);
    }

    public TableView getTable(Long connectionId, String schemaName, String tableName) {
        return runWithConnectionMeta(connectionId, meta -> {
            String tableType = getTableType(meta, schemaName, tableName);
            List<ColumnView> columns = getColumns(meta, schemaName, tableName);
            List<IndexView> indices = getIndices(meta, schemaName, tableName);
            return new TableView(tableName, tableType, columns, indices);
        });
    }

    public TableDataView getTableData(Long connectionId, String schemaName, String tableName, int limit) {
        DataSource ds = connectionService.getDataSource(connectionId);
        try (Connection connection = ds.getConnection()) {

            // Not exactly beautiful, and postgres specific, but some escaping is needed here to prevent SQL injection
            String sql = String.format("select * from %s.%s limit %d",
                    Utils.escapeIdentifier(null, schemaName),
                    Utils.escapeIdentifier(null, tableName),
                    limit);
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                List<Map<String, Object>> rows = mapResultSet(stmt.executeQuery(), (rs, rowNum) -> {
                    Map<String, Object> data = new HashMap<>();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        String name = rs.getMetaData().getColumnName(i);
                        data.put(name, rs.getObject(name));
                    }
                    return data;
                });
                return new TableDataView(tableName, schemaName, rows);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get database metadata", e);
        }
    }

    private List<ColumnView> getColumns(DatabaseMetaData metaData, String schemaName, String tableName) throws SQLException {

        Set<String> pkColumns = getPrimaryKeyColumns(metaData, schemaName, tableName);

        ResultSet rsCols = metaData.getColumns(null, schemaName, tableName, null);
        return mapResultSet(rsCols, (rs, rowNum) ->
                new ColumnView(
                        rs.getString(COLUMN_NAME),
                        rs.getString(COLUMN_TYPE),
                        JDBCType.valueOf(rs.getInt(COLUMN_JDBCTYPE)).name(),
                        rs.getInt(COLUMN_SIZE),
                        NULLABLE_CONST.equals(rs.getString(COLUMN_NULLABLE)),
                        rs.getString(COLUMN_DEFAULT),
                        pkColumns.contains(rs.getString(COLUMN_NAME))));
    }

    private String getTableType(DatabaseMetaData metaData, String schemaName, String tableName) throws SQLException {
        ResultSet rsTable = metaData.getTables(null, schemaName, tableName, tableTypes);
        List<String> tableType = mapResultSet(rsTable, (rs, rowNum) -> rs.getString(TABLE_TYPE));
        return tableType.size() != 0 ? tableType.get(0) : null;
    }

    private Set<String> getPrimaryKeyColumns(DatabaseMetaData metaData, String schemaName, String tableName) throws SQLException {
        ResultSet pkRs = metaData.getPrimaryKeys(null, schemaName, tableName);
        List<String> columns = mapResultSet(pkRs, (rs, rowNum) -> rs.getString("COLUMN_NAME"));
        return new HashSet<>(columns);
    }

    private List<IndexView> getIndices(DatabaseMetaData metaData, String schemaName, String tableName) throws SQLException {
        ResultSet indexRs = metaData.getIndexInfo(null, schemaName, tableName, false, false);
        List<IndexView> indices = mapResultSet(indexRs, (rs, rowNum) ->
                new IndexView(rs.getString(INDEX_NAME), !rs.getBoolean(INDEX_NON_UNIQUE), new ArrayList<>(Collections.singletonList(rs.getString(COLUMN_NAME)))));
        Map<String, List<IndexView>> groupped = indices.stream().collect(Collectors.groupingBy(IndexView::getName));

        return groupped.values().stream().map(
                list -> list.stream().reduce((i1, i2) -> {
                    i1.getColumns().addAll(i2.getColumns());
                    return i1;
                }).orElse(null))
                .collect(Collectors.toList());
    }

    /**
     * Maps the result set to list using given RowMapper
     *
     * @param resultSet result set
     * @param mapper mapper
     * @param <T> result type
     * @return
     * @throws SQLException
     */
    private <T> List<T> mapResultSet(ResultSet resultSet, RowMapper<T> mapper) throws SQLException {
        try(ResultSet rs = resultSet) {
            return new RowMapperResultSetExtractor<>(mapper)
                    .extractData(rs);
        }
    }

    /**
     * Helper method that wraps connection management.
     *
     * @param connectionId id of the connection
     * @param action callback that uses the metadata
     * @param <T> result type
     * @return
     */
    private <T> T runWithConnectionMeta(Long connectionId, MetaDataProcessor<T> action) {
        DataSource ds = connectionService.getDataSource(connectionId);
        try (Connection connection = ds.getConnection()) {
            return action.apply(connection.getMetaData());
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get database metadata", e);
        }
    }

    private interface MetaDataProcessor<T> {
        T apply(DatabaseMetaData metaData) throws SQLException;
    }
}
