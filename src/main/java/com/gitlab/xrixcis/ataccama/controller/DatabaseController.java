package com.gitlab.xrixcis.ataccama.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.gitlab.xrixcis.ataccama.service.DatabaseMetadataService;
import com.gitlab.xrixcis.ataccama.view.SchemaView;
import com.gitlab.xrixcis.ataccama.view.TableDataView;
import com.gitlab.xrixcis.ataccama.view.TableView;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/database")
public class DatabaseController {

    private final DatabaseMetadataService service;

    public DatabaseController(DatabaseMetadataService service) {
        this.service = service;
    }

    @GetMapping("/{connectionId}/schema")
    public List<String> listSchemas(@PathVariable("connectionId") Long connectionId) {
        return service.listSchemas(connectionId);
    }

    @JsonView(TableView.ShortView.class)
    @GetMapping("/{connectionId}/schema/{schemaName}")
    public SchemaView getSchema(@PathVariable("connectionId") Long connectionId,
                                @PathVariable("schemaName") String schemaName) {
        return service.getSchema(connectionId, schemaName);
    }

    @JsonView(TableView.FullView.class)
    @GetMapping("/{connectionId}/schema/{schemaName}/table/{tableName}")
    public TableView getSchema(@PathVariable("connectionId") Long connectionId,
                               @PathVariable("schemaName") String schemaName,
                               @PathVariable("tableName") String tableName) {
        return service.getTable(connectionId, schemaName, tableName);
    }

    @GetMapping("/{connectionId}/schema/{schemaName}/table/{tableName}/data")
    public TableDataView getDataPreview(@PathVariable("connectionId") Long connectionId,
                                        @PathVariable("schemaName") String schemaName,
                                        @PathVariable("tableName") String tableName,
                                        @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return service.getTableData(connectionId, schemaName, tableName, limit);
    }
}
