package com.gitlab.xrixcis.ataccama.controller;

import com.gitlab.xrixcis.ataccama.service.ConnectionService;
import com.gitlab.xrixcis.ataccama.view.ConnectionView;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/connection")
public class ConnectionController {

    private final ConnectionService connectionService;

    public ConnectionController(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @GetMapping
    public List<ConnectionView> list() {
        return connectionService.listAll();
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody ConnectionView connection, Errors errors) {
        if (errors.hasErrors()) {
            // TODO: return errors
            return ResponseEntity.badRequest().build();
        }
        Long id = connectionService.store(connection);
        return ResponseEntity.created(URI.create("/v1/connection/" + id)).build();
    }

    @GetMapping("/{id}")
    public ConnectionView get(@PathVariable("id") Long id) {
        return connectionService.get(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody ConnectionView connection, Errors errors) {
        if (errors.hasErrors()) {
            // TODO: return errors
            return ResponseEntity.badRequest().build();
        }
        connectionService.update(id, connection);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        connectionService.delete(id);
    }
}
