package com.gitlab.xrixcis.ataccama.view;

import lombok.Data;

@Data
public class ColumnView {

    public final String name;

    public final String type;

    public final String jdbcType;

    public final int size;

    public final boolean nullable;

    public final String defaultValue;

    public final boolean primaryKey;
}
