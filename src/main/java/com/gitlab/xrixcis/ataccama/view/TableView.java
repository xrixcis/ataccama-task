package com.gitlab.xrixcis.ataccama.view;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import java.util.List;

@Data
@JsonView(TableView.FullView.class)
public class TableView {

    public interface ShortView {}

    public interface FullView extends ShortView {}

    @JsonView(TableView.ShortView.class)
    private final String name;

    @JsonView(TableView.ShortView.class)
    private final String type;

    private final List<ColumnView> columns;

    private final List<IndexView> indices;

    public TableView(String name, String type) {
        this.name = name;
        this.type = type;
        this.columns = null;
        this.indices = null;
    }

    public TableView(String name, String type, List<ColumnView> columns, List<IndexView> indices) {
        this.name = name;
        this.type = type;
        this.columns = columns;
        this.indices = indices;
    }
}
