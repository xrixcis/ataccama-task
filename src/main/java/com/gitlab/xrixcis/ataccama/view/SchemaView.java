package com.gitlab.xrixcis.ataccama.view;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class SchemaView {

    private final String name;

    private final List<TableView> tables;
}
