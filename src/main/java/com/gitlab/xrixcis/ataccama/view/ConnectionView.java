package com.gitlab.xrixcis.ataccama.view;

import com.gitlab.xrixcis.ataccama.entity.Connection;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class ConnectionView {

    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String host;

    @NotNull
    @Positive
    @Max(65535)
    private Integer port;

    @NotBlank
    private String database;

    @NotBlank
    private String username;

    private String password;

    public ConnectionView() {
    }

    public ConnectionView(Connection connection) {
        id = connection.getId();
        name = connection.getName();
        host = connection.getHost();
        port = connection.getPort();
        database = connection.getDatabase();
        username = connection.getUsername();
        password = "******";
    }
}
