package com.gitlab.xrixcis.ataccama.view;

import lombok.Data;

import java.util.List;

@Data
public class IndexView {

    private final String name;

    private final boolean unique;

    private final List<String> columns;
}
