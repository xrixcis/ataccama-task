package com.gitlab.xrixcis.ataccama.view;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TableDataView {

    private final String name;

    private final String schema;

    private final List<Map<String, Object>> data;
}
