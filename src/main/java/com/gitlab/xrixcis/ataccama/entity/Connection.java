package com.gitlab.xrixcis.ataccama.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Connection entity, stores all info necessary to connect to a database;.
 */
@Entity
@Data
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String host;

    @Column(nullable = false)
    private Integer port;

    @Column(nullable = false)
    private String database;

    @Column(nullable = false)
    private String username;

    private String password;

    public Connection() {
    }

    public Connection(String name, String host, Integer port, String database, String username, String password) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }
}
