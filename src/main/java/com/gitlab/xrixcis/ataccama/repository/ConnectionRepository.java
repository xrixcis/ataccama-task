package com.gitlab.xrixcis.ataccama.repository;

import com.gitlab.xrixcis.ataccama.entity.Connection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Connection repository
 */
@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {
}
